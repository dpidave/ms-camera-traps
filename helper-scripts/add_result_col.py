#!/usr/bin/env python
import csv
import sys

# a script that uses the manually tagged file name to 
# add a FP, FN, TP, TN tag in a result column, based on the 
# probability cut-off set at the top of this file. 
# The prob cut-off is reported as the max_probability of an 
# animal based on the model. In this way I can use the notebook
# script to make probability curves and calculate a proper
# matrix

# the script should be run from the base of the git repo
# so that it can find the files

# passthe script the megadetector csv file
# python megadetector.csv

def assign_result(species_id, prob, cutoff):
    # animal in image based on model
    if prob >= cutoff:
        if species_id == 0:
            return 'FP'
        else:
            return 'TP'
    else:
        # no animal in image based on model
        if species_id == 0:
            return 'TN'
        else:
            return 'FN'

# set cut-off here
cutoff = 0.8 # 80%
# the tag file in the camera_trap_data directory
label_data = './camera_trap_images/GrooteResult_all.csv'

# key: filename, value: [SpeciesID, species-name]
with open(label_data) as rf:
    csv_reader = csv.reader(rf)
    header = next(csv_reader)
    tag_dict = dict(((cols[1].upper(), [cols[5], cols[6]])
                     for cols in csv_reader))

print(tag_dict['GE-1C1_00175_11-05-2016_19-38.JPG'])

# the csv file from megadetector
run_file = sys.argv[1]
out_file = run_file.replace('.csv', '_tagged.csv')
print("Writing tagged data too %s" % out_file)

with open(out_file, 'w') as wf:
    csv_writer = csv.writer(wf)
    with open(run_file) as rf:
        csv_reader = csv.reader(rf)
        model_header = next(csv_reader)
        model_header.insert(2, 'species_name')
        model_header.insert(2, 'result')
        csv_writer.writerow(model_header)
        for row in csv_reader:
            file_target = row[0].split('/')[-1].upper()
            max_confidence = float(row[1])
            try:
                species_id = int(tag_dict[file_target][0])
                species_name = tag_dict[file_target][1]
                row.insert(2, species_name)
                result = assign_result(species_id, max_confidence, cutoff)
                row.insert(2, result)
                csv_writer.writerow(row)
            except KeyError:
                print('file %s has no data in %s' % (file_target, 
                                                     label_data))
            except ValueError:
                print('file %s has no data in %s' % (file_target, 
                                                     label_data))
