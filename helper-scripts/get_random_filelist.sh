#!/bin/bash

# grab 50k of random lines from a list of files names
# Input example command
# find * -name *.JPG -type f > file_list.txt
# to run this script
# bash get_random_filenames.sh file_list.txt 50000
# You can change the 50000 to any number 
# Just redirect the output to a file and then loop through that
# with the cp command. Easy peasy!

shuf -n $2 $1
