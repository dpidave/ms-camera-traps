#!/bin/bash

## Needs to run from within CameraTrap directory with the conda
## env activated
## Image_dir is from stdin

#/ms-camera-traps/megadetector_v3.pb

start=`date +%s`
echo "starting at $start"
python ../ms-camera-traps/run_tf_detector.py md_v4.1.0.pb --recursive --image_dir "$1"
end=`date +%s`

runtime=$((end-start))
echo "runtime was $runtime seconds"
