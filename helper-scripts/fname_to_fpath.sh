#!/bin/bash

# use the following for testing, with the -n flag
#find ./* -type f -name '*.JPG' -exec prename -n 's:^./::; s:/:_:g; s:^::' {} +

# this is the command without the -n flag
find ./* -type f -name '*.JPG' -exec prename 's:^./::; s:/:_:g; s:^::' {} +
