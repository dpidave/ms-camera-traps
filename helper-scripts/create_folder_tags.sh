#!/bin/bash # sort images based on a csv file into true and false triggers
TRUETRIG='positive'
FALSETRIG='negative'

TPDIR='TP'
TNDIR='TN'
FPDIR='FP'
FNDIR='FN'
# cut off as a probability of successful image detection
# this will be used to check either positive or neg dirs for file
cutoff=0.8

mkdir -p $TPDIR
mkdir -p $TNDIR
mkdir -p $FPDIR
mkdir -p $FNDIR

echo "Probability cut-off set at $cutoff"

IFS=","
while read file result tag junk
do
  # case causes issues here, not sure how to fix at this stage
  TARGET=$(basename $file .JPG)_detections.jpg
  TARGETUP=$(echo $TARGET | tr A-Z a-z)
  if (( $(echo "$result > $cutoff" | bc -l) ))
  then
    # in true trigger dir either true or false positive
    if [ "$tag" = "TP" ]; then
      mv $TRUETRIG/"$TARGETUP" $TPDIR
    else
      mv $TRUETRIG/"$TARGETUP" $FPDIR
    fi
  else
    # in negative dir, either true neg or false neg
    if [ "$tag" = "TN" ]; then
      mv $FALSETRIG/"$TARGETUP" $TNDIR
    else
      mv $FALSETRIG/"$TARGETUP" $FNDIR
    fi
  fi
done<"$1"
