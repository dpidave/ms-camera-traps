# MS-Camera traps repo based on MegaDetector   

## ToDo  
1. Update readme  
2. Look at setting boundary box based on user info ie red square
3. Create a script that makes consistant file names to avoid case issues  
4. Add roc curve instructions to notebooks  

## Introduction  
This repo contains a simple modification to the `run_tf_detctor.py` to output
boundary box information.  The basic instructions are from the ms-github page
but I have simplified these below.  

The idea here would be to run this on a batch of images to identify any with
animals, then to use the boundary box information for training on Australian
animals that are likely to be in the images.  

There is another script in the CameraTraps repo in `detection/run_..batch.py` 
which generates a json file containing the image coordinates. I think this will be useful for training
purposes. An example of output from this program is in `output`. To run this use:  

```
python detection/run_tf_detector_batch.py ../mychanges/megadetector_v3.pb
    ../mychanges/test_images/ ../mychanges/outputs/results.json 
``` 

Also, there seem to be a bunch of untilities for generating training
datasets in this repo but they require a bit of setting up re dependencies, so
I haven't tried these yet. See the tutorial on training available in
https://github.com/microsoft/CameraTraps/blob/master/classification/TUTORIAL.md

For example the below code fails due to import issues (I think I need to add a
directory to my python path).  

```
python data_management/databases/classification/make_classification_dataset.py
../mychanges/json/ ../mychanges/test_images/ megadetector_v3.pb
--coco_style_output ../mychanges/outputs/ --tfrecords_ouput
../mychanges/tfoutput/ --location_key location
```  

## Instructions  
- Clone the required repos  
```
git clone https://github.com/Microsoft/cameratraps   
git clone https://github.com/Microsoft/ai4eutils  
``` 

- Add the ai4eutils to your python path (add this to `~/.bashrc`)   
```
export PYTHONPATH="$PYTHONPATH:/full/path/to/ai4eutils/"
```  
Use `source ~/.bashrc` to reload the file  

- Use conda to creat the python environment  

```
# detector packages
conda env create -f environment-detector.yml   
# activate the env   
conda activate cameratraps-detector  
```

- Download my version of `run_tf_detector.py` to the CameraTraps directory  

- Download the lattest weights file  
``` 
# ~300MB  
wget https://lilablobssc.blob.core.windows.net/models/camera_traps/megadetector/megadetector_v3.pb 
```

- Run the classifier on a set of test images in `test_images`  
```
python run_tf_detector.py megadetector_v3.pb --image_dir test_images/
```

The above will save the results as the infile name ending in `_detectors.jpg`.  

A file called `bbox_log.txt` contains the boundary box coordinates, which are
x1, y1, width, height, as percentage of the image size.
